import { Dialect } from "sequelize/types";

export interface DBParams{
    user:string;
    pass:string;
    db:string;
    params:ParamsConnectedDB;
}

export interface ParamsConnectedDB{
    host:string;
    port?:number;
    dialect:Dialect;
    pool?:PoolParameters;
}
export interface PoolParameters{
    max: number;
    min: number;
    acquire: number;
    idle: number;
}
