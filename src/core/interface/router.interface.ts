import {Router,Application} from 'express';

export interface routerInterface {
    router:Router;
    endPointBase:String;
    loadRouter():void;
}