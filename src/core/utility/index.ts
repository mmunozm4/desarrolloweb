import path from "path";
import fs, { accessSync, existsSync } from "fs";
//import {initConfig} from "../../config"
import config from "config";
import { Request } from "express";

export class utility {
  static async exists(pathFile: string): Promise<Boolean> {
    try {
      console.log("pathFile");
      console.log(pathFile);
      console.log("fs.existsSync(pathFile)");
      console.log(fs.existsSync(pathFile));
      console.log("fs.accessSync(pathFile)");
      console.log(fs.accessSync(pathFile));
      console.log(fs.statSync(pathFile));

      let existFile = fs.existsSync(pathFile) && fs.accessSync(pathFile);
      return true;
    } catch {
      return false;
    }
  }

  static async rootFiles(
    pathFiles: string,
    directory: boolean = true
  ): Promise<string[]> {
    let _rootFiles: string[] = [];
    try {
      await fs
        .readdirSync(pathFiles, { withFileTypes: true })
        .filter((dirent) => dirent.isDirectory() == directory)
        .forEach(async (file) =>
          _rootFiles.push(path.resolve(pathFiles, file.name))
        );
    } catch (error) {
      _rootFiles = new Array();
    }
    return _rootFiles;
  }

  static validString(
    val: string,
    lenth: number,
    numers: boolean = false,
    caract: boolean = false
  ) {
    //let re = new RegExp(`^([a-z0-9]{${lenth},})$`);
    let re = new RegExp(
      `^([A-Za-z${numers ? "0-9" : ""}${
        caract ? "\\[«#$%&/(),.=*-\\]\\s" : ""
      }]{${lenth},})$`
    );
    return re.test(val) || typeof val === "undefined";
  }

  static buildCode(code: number,leng:number=<number>config.get("initConfig.LENGTH_CODE")): string {
    let c = code.toString();
    let l = c.length;
    for (let x = l; x < leng; x++) c = "0" + c;
    return c;
  }
  static getBaseAtributes(req:Request,T:number = 1):object{
    if(T == 1) 
    return{
      creado : new Date(),
      creado_por : 1,
      actualizado : new Date(),
      actualizado_por : 1,
      TRLID : 1
    }
    else if(T==2) 
    return{
      actualizado : new Date(),
      actualizado_por : 1,
    }
    else 
    return{
      actualizado : new Date(),
      actualizado_por : 1,
      eliminado:new Date(),
      eliminado_por:1,
      TRLID : 3
    }
  }
  static getPaginationParams(quer: any) {
    let limit = null,
      offset = null;
    if (quer.pagination) {
      limit = parseInt(quer.limit?.toString() || "10");
      offset = parseInt(quer.offset?.toString() || "0");
      return {
        limit,
        offset,
      };
    }else return {};
  }
  static getId(quer: any, name: string = "id"): any {
    return {
      id: parseInt(quer[name]?.toString() || "0"),
    };
  }
  static throwExpression(errorMessage: string | number): never {
    let errorMessageResponser: string;
    if (typeof errorMessage === "number") {
      let file = require(`${config.get(
        "initConfig.APP_ROOT"
      )}/messages/${config.get("app.language")}`);
      errorMessageResponser = file["default"][errorMessage];
      throw new Error(errorMessageResponser);
    } else {
      errorMessageResponser = <string>errorMessage;
      throw new Error(errorMessageResponser);
    }
  }

  static renameKeys(obj: any, newKeys: any) {
    const keyValues = Object.keys(obj).map((key) => {
      const newKey = newKeys[key] || key;
      return { [newKey]: obj[key] };
    });
    return Object.assign({}, ...keyValues);
  }
}

export * from './mimeType'
