export const Messages:{[index: string]:any} = {
  200:'ok',
  201:'Registro Creado',
  400:'Error Al Procesar La Solicitud',
  401:'Recurso No Autorizado',
  403:'Solicitud Rechazada',
  404:'Recurso No Encontrado',
  405:'Metodo No Permitido',
  406:'Solicitud No Aceptada',
  408:'Tiempo de Respuesta Excedido',
  500:'Error Dentro Del Sistema',
  502:'Respuesta invalidad del sevicio'
}