import {ICore,utility} from '../'
//import {initConfig} from '../../config'
import config from 'config';
import {  Response } from 'express';
import { Messages } from './Mesagge';

export class  httpResponse{
    public static sendResponse(data:ICore.resInterface,res:Response){
        data.cod_sis = utility.validString(data.cod_sis!,0,true,true)?config.get('initConfig.CODE_ERROR') +'-'+ utility.buildCode(data.code):data.cod_sis;
        data.mensaje = utility.validString(data.mensaje!,0,true,true)?data.mensaje:Messages[data.code.toString()];


        if(config.get('initConfig.HTTPCODERESPONSE'))
            res.status(data.code).send(data);
        else 
            res.status(200).send(data);
    }
}