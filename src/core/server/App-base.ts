import path from "path";
import fs from "fs";

import express, { Request, Response } from "express";
import Compression from "compression";
import morgan from "morgan";
import helmet from "helmet";
import Cors from "cors";
import cookieParser from "cookie-parser";

import { ICore,utility } from '../';
//import {initConfig} from '../../config';
import config  from 'config';

export class App_base implements ICore.App_interface {
    app: express.Application = express();

    constructor() {
        this.config();
        this.loadModulos();
        //this.initRoute();
    }

    // set base parameteres.
    protected config() {
        // support application/json type post data
        this.app.use(express.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(express.urlencoded({ extended: true }));
        //support Compresion response GZIP
        this.app.use(Compression({}));
        // secure complemets
        this.app.use(helmet());
        this.app.use(Cors());
        this.app.use(cookieParser());
        this.app.use('/static', express.static(path.resolve(config.get('initConfig.APP_ROOT'),'../public')));
    }

    protected async loadModulos():Promise<void>{
        let modules:string[]= await utility.rootFiles(path.resolve(config.get('initConfig.APP_ROOT'),'./components'),true);
        let routes:string[],controllers:string[];
        modules.forEach(async(module)=>{
            routes = await utility.rootFiles(path.resolve( module,'./routers'),false);
            if(routes.length > 0 ){
                for (const route of routes) {
                    let routerFile =  path.basename(route).replace('.router.ts','');
                    if(routerFile == "index")
                        routerFile = path.basename(module);
                    else
                        routerFile = `${path.basename(module)}/${routerFile}`;
                    await import(route).then(async (module) => {
                        let m = new module.default(this.app,`/${routerFile.toLowerCase()}`);
                        return m;
                    });
                }
                /*routes.forEach(async(route)=>{
                    let routerFile =  path.basename(route).replace('.router.ts','');
                    if(routerFile == "index")
                        routerFile = path.basename(module);
                    else
                        routerFile = `${path.basename(module)}/${routerFile}`;
                    await import(route).then(async (module) => {
                        let m = new module.default(this.app,`/${routerFile.toLowerCase()}`);
                        return m;
                    });
                });*/
            }
            controllers = await utility.rootFiles(path.resolve( module,'./controllers'),true);
            if(controllers.length > 0){
                
                controllers = await controllers.filter((con) =>{
                    return fs.existsSync(path.resolve(con,'./router.ts'));
                });
                for (const controller of controllers) {
                    let uri = await `${path.basename(module)}/${path.basename(controller)}`;
                    let pathFile = await path.resolve( controller,`./router`);
                    await import(pathFile).then(async (RCM) => {
                        let m = new RCM.default(this.app,`/${uri.toLowerCase()}`);
                        return m;
                    });   
                }
               /* controllers.forEach(async(controller)=>{
                    uri =   `${path.basename(module)}/${path.basename(controller)}`;
                    pathFile = await path.resolve( controller,`./router.ts`);
                    await import(pathFile).then(async (RCM) => {
                        console.log(uri);
                        let m = new RCM.default(this.app,`/${uri.toLowerCase()}`);
                        return m;
                    });                    
                });*/
            }
        });
    }

    protected initRoute() {
        this.app.get('/', (req: Request, res: Response, next) => res.json('hola mundo'));
    }

    public listen(port:number){
        this.app.listen(port,()=>{
            console.log(`La aplicación está escuchando en:
                        http://localhost:${port}`);
        })
    }
}