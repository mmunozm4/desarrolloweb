import { Sequelize, Dialect } from "sequelize";
import { ICore } from "../core";
//import {DBConfig} from '../config'
import config from "config";
import { any } from "bluebird";
/*export const conDBOppor:Sequelize = new Sequelize(DBConfig.Oppor.db,DBConfig.Oppor.user,DBConfig.Oppor.pass,{
host:DBConfig.Oppor.params.host,
dialect:"postgres" //DBConfig.Oppor.params.Dialect    
});*/

let DBConfig: ICore.DBParams = config.get("dataBases.Oppor");
export const conDBOppor: Sequelize = new Sequelize(
  DBConfig.db,
  DBConfig.user,
  DBConfig.pass,
  {
    host: DBConfig.params.host,
    dialect: <Dialect>DBConfig.params.dialect,
  }
);

conDBOppor.authenticate();
