import { personaDTO } from "./persona.dto";
import { Request, Response } from "express";
import { Op } from "sequelize";
import { utility, httpResponse, ICore } from "../../../../core";
import { conDBOppor } from "../../../../instances";


import { Persona } from "../../models";
export class PersonaController extends personaDTO {
  public async get(req: Request, res: Response) {
    let pagination = utility.getPaginationParams(req.query);
    if (req.query!)
      if (req.query.id!)
        res.send(
          await Persona.findByPk(parseInt(req.query.id.toString()), {
            ...pagination,
            attributes: personaDTO.columnsBase,
          })
        );
      else
        res.send(
          await Persona.findAll({
            ...pagination,
            
            attributes: personaDTO.columnsBase,
          })
        );
  }
  public async index(req: Request, res: Response) {
    try {
      let pagination = utility.getPaginationParams(req.query);
      if (req.query!)
        if (req.query.id!) {
          let data: Persona | null = await Persona.findByPk(
            parseInt(req.query.id.toString()),
            {
              ...pagination,
              raw: true,
            }
          );
          httpResponse.sendResponse(
            { code: 200, data: data ? [data] : [], mensaje: "" },
            res
          );
        } else {
          let data: Persona[] = await Persona.findAll({
            where: {
              TRLID: {
                [Op.ne]: 3,
              },
            },
            ...pagination,
            raw: true,
          });
          httpResponse.sendResponse({ code: 200, data, mensaje: "" }, res);
        }
    } catch (err: any) {
      console.log(err);
      httpResponse.sendResponse({ code: 500, mensaje: err.message }, res);
    }
  }
  public async add(req: Request, res: Response) {
    try{
      let Persona = await Persona.create(req.body);
      await Object.assign(Persona,utility.getBaseAtributes(req,1));
      (await Persona).save();
      httpResponse.sendResponse({code:200, data:[Persona],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let Persona = await Persona.findByPk(parseInt(req.query.id.toString()));
        if(Persona){
          await Object.assign(Persona,utility.getBaseAtributes(req, 3));
          Persona.save();
          httpResponse.sendResponse({code:200,data:[Persona]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let Persona = await Persona.findByPk(parseInt(req.body.id.toString()));
          if (Persona) {
            await Object.assign(Persona,utility.getBaseAtributes(req, 2));
            Persona.status_id = parseInt(req.body.status_id?.toString() || "");
            Persona.pagina_id = parseInt(req.body.pagina_id?.toString() || "");
            Persona.metodo_id = parseInt(req.body.metodo_id?.toString() || "");
            Persona.descripcion = req.body.descripcion?.toString() || "";
            Persona.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[Persona]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}
