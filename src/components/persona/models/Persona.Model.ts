import { Model, DataTypes, NOW} from "sequelize";
import { conDBOppor } from "../../../instances";

export interface PersonaAttributes {
  id?: number;
  nombre1: string;
  nombre2?: string;
  apellido1: string;
  apellido2?: string;
  apellido_casada?:string;
  fecha_nacimiento:Date;
  dni:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface personaAdd {
  nombre1: string;
  nombre2?:string;
  apellido1: string;
  apellido2?:string;
  apellido_casada?:string;
  fecha_nacimiento:Date;
  dni:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  TRLID?: number;
}
export interface PersonaViewModel {
    nombre1: string;
    nombre2: string;
    apellido1: string;
    apellido2: string;
    apellido_casada:string;
    fecha_nacimiento:Date;
    dni:string;
}
class Persona extends Model<PersonaAttributes, personaAdd> {
  id?: number;
  nombre1!: string;
  nombre2?: string;
  apellido1!: string;
  apellido2?: string;
  apellido_casada?:string;
  fecha_nacimiento!:Date;
  dni!:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

Persona.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre1: DataTypes.STRING(80),
    nombre2: DataTypes.STRING(80),
    apellido1: DataTypes.STRING(80),
    apellido2: DataTypes.STRING(80),
    apellido_casada:DataTypes.STRING(90),
    fecha_nacimiento:DataTypes.DATE,
    dni:DataTypes.STRING(90),
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "persona",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "escolaridad",
  }
);
//User.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});
//User.belongsTo(TipoUser,{foreignKey:'tipo_user_id', targetKey:'id'});

export { Persona };
