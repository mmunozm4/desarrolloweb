import { Model, DataTypes, NOW} from "sequelize";
import { conDBOppor } from "../../../instances";

export interface AlumnoAttributes {
  id?: number;
  carrera:string;
  facultad:string;
  personaid:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface AlumnoAdd {
  carrera:string;
  facultad:string;
  personaid:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  TRLID?: number;
}
export interface AlumnoViewModel {
    carrera:string;
    facultad:string;
    personaid:string;
}
class Alumno extends Model<AlumnoAttributes, AlumnoAdd> {
  id?: number;
  carrera!:string;
  facultad!:string;
  personaid!:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

Alumno.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    carrera:DataTypes.STRING(90),
    facultad:DataTypes.STRING(90),
    personaid:DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "Alumno",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "escolaridad",
  }
);
//User.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});
//User.belongsTo(TipoUser,{foreignKey:'tipo_user_id', targetKey:'id'});

export { Alumno };
