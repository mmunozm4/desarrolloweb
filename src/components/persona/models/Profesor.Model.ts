import { Model, DataTypes, NOW} from "sequelize";
import { conDBOppor } from "../../../instances";

export interface ProfesorAttributes {
  id?: number;
  tipo_pension:string;
  personaid:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface ProfesorAdd {
  tipo_pension:string;
  personaid:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  TRLID?: number;
}
export interface ProfesorViewModel {
    tipo_pension:string;
    personaid:string;
}
class Profesor extends Model<ProfesorAttributes, ProfesorAdd> {
  tipo_pension!:string;
  personaid!:string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

Profesor.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    tipo_pension:DataTypes.STRING(90),
    personaid:DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "Profesor",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "escolaridad",
  }
);
//User.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});
//User.belongsTo(TipoUser,{foreignKey:'tipo_user_id', targetKey:'id'});

export { Profesor };
