import { Request, Response } from "express";
import { IncomingHttpHeaders } from "http";
import { authController } from "../controllers/auth.controller";
import {User} from '../../admin/models';

export class authorizationMiddleware {
  static async checkLogin(req: Request, res: Response, next: Function) {
      let headersUser;
      const token: string =
        authorizationMiddleware.getTokenFromHeaders(req.headers) ||
        req.query.token ||
        req.body.token ||
        "";
      try{
        if (token.length > 0) {
          headersUser = <User>await authController.validToken(token);
            res.locals.user = await User.findByPk(headersUser.id);
            next();
        }else res.status(401).send('Unauthorized token invalido');   
      }catch(errors){
            if(errors.message == 'jwt expired'){
              try{ 
              let payload = (await authController.DecodeToken(token))!.payload;
              delete payload.iat;
              delete payload.exp;
              let newToken = await authController.getNewToken(payload);
              req.headers.authorization = "Bearer " + newToken;
              res.locals.user = await User.findByPk(payload.id);
              next();
              }catch(error){
                res.status(400).send('Bad Request '+error.message)
              }
            }else
              res.status(403).send('Forbidden '+errors);   
      }
  }
  static async checkAuthorization(req: Request, res: Response, next: Function) {
    console.log('checkAuthorization');
    next();
  }

  static getTokenFromHeaders(headers: IncomingHttpHeaders) {
    const header = headers.authorization as string;
    if (!header) return header;
    return header.split(" ")[1];
  }
}