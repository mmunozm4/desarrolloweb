import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken'
import config from 'config';

import {Persona} from '../../persona/models';
import {User,TipoUser, userValidator, userAttributesForToken, userAdd} from '../../admin/models';
import {utility,httpResponse,ICore} from '../../../core';
import {conDBOppor} from '../../../instances';
//import {initConfig} from '../../../config';

export class authController {
	private static readonly _saltRounds =<number>config.get('session.SALTROUNDS');
	private static readonly _jwtSecret:jwt.Secret  = <jwt.Secret>config.get('session.JWTSECRET');



	public static async encriptPass(pass:string):Promise<string>{
		return await bcrypt.hashSync(pass, await bcrypt.genSaltSync(authController._saltRounds) );
	}

	public async registerBasic (req: Request, res: Response) {
		let persona =  <Persona>req.body.persona;
		let user =  <User>req.body.user;

		let errors:string = "";
		errors+= utility.validString(persona.nombre1,3)?"":"Nombre1 necesario para el ingreso y debe de ser mayor a 3 caracteres";
		errors+= utility.validString(persona.apellido1,3)?"":"\\n Apellido1 necesario para el ingreso y debe de ser mayor a 3 caracteres";

		errors+= (await authController.UserValidator(user)).errors;

		if(utility.validString(errors,0) ){
			let t = await conDBOppor.transaction();
			try{
				persona.creado = new Date();
				persona.creado_por = 1;
				persona.actualizado = new Date();
				persona.actualizado_por = 1;
				persona.TRLID = 1;
				persona = await Persona.create(persona,{transaction:t});

				let TUAdmin = await TipoUser.findOne({where:{
					nombre:'Admin'
				},transaction:t});
				
				user.persona_id = persona.id!;
				user.creado = new Date();
				user.creado_por = 1;
				user.actualizado = new Date();
				user.actualizado_por = 1;
				user.tipo_user_id = TUAdmin?.id!;
				user.status_id = 1;
				user.password = await authController.encriptPass(user!.password!);
				user.TRLID = 1;
				user  = await User.create(user,{transaction:t});

				await t.commit();
				httpResponse.sendResponse({code:200, data:[persona,user]},res);
			}catch(err:any){
				await t.rollback();
				httpResponse.sendResponse({code:406, data:new Array(),mensaje:err.messaje},res);
			}
		}else
			httpResponse.sendResponse({code:406, data:new Array(),mensaje:errors},res);
	}
	
	public async login(req: Request, res: Response) {
		let resData:ICore.resInterface;
		let userReq:User = <User>req.body.user;
		let validUser:userValidator = await authController.UserValidator(userReq);
		if(validUser.valid){
			try{
				//userReq.password = await authController.encriptPass(userReq.password);
				let user = await User.findOne({
					where: {
						"username":userReq.username
					},
					attributes:userAttributesForToken
				});
				let validPass = await bcrypt.compare(userReq.password, user!.password!);
				if(user && validPass){
					let peyload  = <userAdd>user.toJSON();
					delete peyload.password;
					let response = {
						token: await authController.getNewToken(peyload)
					};

					httpResponse.sendResponse({code:200, data:new Array(response)},res);
					// res.send(response);
				}else 
				httpResponse.sendResponse({code:404, data:new Array(),mensaje:'usuario o password invalido'},res);
			}catch(error:any){
				httpResponse.sendResponse({code:400, data:new Array(),mensaje:error.messaje},res);	
			}		
		}else
			httpResponse.sendResponse({code:404, data:new Array(),mensaje:validUser.errors},res);	
	}
	public static async getNewToken(peyload:object):Promise<string>{
		return  await (jwt.sign(peyload,authController._jwtSecret,{
			expiresIn:  <string>config.get('session.EXPIRES_IN'),// 604800, // 1 week
			algorithm: 'HS512'
		} ));		
	}
	public static async DecodeToken(token: string) {
		return jwt.decode(token, { complete: true });
	  }
	

	public static async  UserValidator(user:User):Promise<userValidator>{
		let res:userValidator={
			errors:"",
			valid:false
		};
		res.errors+= utility.validString(user.username,5)?"":"\\n username necesario para el ingreso y debe de ser mayor a 5 caracteres";
		res.errors+= utility.validString(user.password!,8,true)?"":"\\n password 1 necesario para el ingreso y debe de ser mayor a 8 caracteres";
		if(utility.validString(res.errors,0))
			res.valid = true;
		return res;
	}

	public static async validToken(token: string):Promise<Object>{
		try{
			return await jwt.verify(token,authController._jwtSecret);
		}catch(errors){
			throw errors;
		}
	}
	
}