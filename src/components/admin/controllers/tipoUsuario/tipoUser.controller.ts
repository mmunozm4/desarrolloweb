import { Request, Response } from "express";
import {Op} from 'sequelize';
import { httpResponse, utility } from "../../../../core";
import { TipoUser } from "../../models";

export class TipoUserController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:TipoUser|null  = await TipoUser.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:TipoUser[] = await TipoUser.findAll({where:{
          TRLID:{
            [Op.ne]:3
          }
        },
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  
  public async add(req: Request, res: Response) {
    try{
      let tipoUser = await TipoUser.create(req.body);
      await Object.assign(tipoUser,utility.getBaseAtributes(req,1));
      (await tipoUser).save();
      httpResponse.sendResponse({code:200, data:[tipoUser],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let tipoUser = await TipoUser.findByPk(parseInt(req.query.id.toString()));
        if(tipoUser){
          await Object.assign(tipoUser,utility.getBaseAtributes(req, 3));
          tipoUser.save();
          httpResponse.sendResponse({code:200,data:[tipoUser]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let tipoUser = await TipoUser.findByPk(parseInt(req.body.id.toString()));
          if (tipoUser) {
            await Object.assign(tipoUser,utility.getBaseAtributes(req, 2));
            tipoUser.nombre = req.query.nombre?.toString() || "";
            tipoUser.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[tipoUser]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}
