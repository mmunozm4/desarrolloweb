import { Request, Response } from "express";
import { httpResponse, utility } from "../../../../core";
import { TipoMetodo } from "../../models";
export class TipoMetodoController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:TipoMetodo|null  = await TipoMetodo.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:TipoMetodo[] = await TipoMetodo.findAll({
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  
  public async add(req: Request, res: Response) {
    try{
      let tipoMetodo = await TipoMetodo.create(req.body);
      await Object.assign(tipoMetodo,utility.getBaseAtributes(req,1));
      (await tipoMetodo).save();
      httpResponse.sendResponse({code:200, data:[tipoMetodo],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let tipoMetodo = await TipoMetodo.findByPk(parseInt(req.query.id.toString()));
        if(tipoMetodo){
          await Object.assign(tipoMetodo,utility.getBaseAtributes(req, 3));
          tipoMetodo.save();
          httpResponse.sendResponse({code:200,data:[tipoMetodo]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let tipoMetodo = await TipoMetodo.findByPk(parseInt(req.body.id.toString()));
          if (tipoMetodo) {
            await Object.assign(tipoMetodo,utility.getBaseAtributes(req, 2));
            tipoMetodo.nombre = req.body.nombre?.toString() || "";
            tipoMetodo.statusId = req.body.statusId?.toString() || "";
            tipoMetodo.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[tipoMetodo]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}