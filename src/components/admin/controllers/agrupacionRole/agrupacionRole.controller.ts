import { Request, Response } from "express";
import {Op} from 'sequelize';
import { httpResponse, utility } from "../../../../core";
import { GrupoRole } from "../../models";
export class GrupoRoleController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:GrupoRole|null  = await GrupoRole.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:GrupoRole[] = await GrupoRole.findAll({where:{
          TRLID:{
            [Op.ne]:3
          }
        },
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }  
  public async add(req: Request, res: Response) {
    try{
      let grupoRole = await GrupoRole.create(req.body);
      await Object.assign(grupoRole,utility.getBaseAtributes(req,1));
      (await grupoRole).save();
      httpResponse.sendResponse({code:200, data:[grupoRole],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let grupoRole = await GrupoRole.findByPk(parseInt(req.query.id.toString()));
        if(grupoRole){
          await Object.assign(grupoRole,utility.getBaseAtributes(req, 3));
          grupoRole.save();
          httpResponse.sendResponse({code:200,data:[grupoRole]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.query!)
        if (req.query.id!) {
          let grupoRole = await GrupoRole.findByPk(parseInt(req.body.id.toString()));
          if (grupoRole) {
            await Object.assign(grupoRole,utility.getBaseAtributes(req, 2));
            grupoRole.nombre = req.body.nombre?.toString() || "";
            grupoRole.status_id = req.body.status_id?.toString() || "";
            grupoRole.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[grupoRole]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}