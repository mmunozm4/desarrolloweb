import { Request, Response } from "express";
import { httpResponse, utility } from "../../../../core";
import { Role } from "../../models";
export class RoleController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:Role|null  = await Role.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:Role[] = await Role.findAll({
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async add(req: Request, res: Response) {
    try{
      let role = await Role.create(req.body);
      await Object.assign(role,utility.getBaseAtributes(req,1));
      (await role).save();
      httpResponse.sendResponse({code:200, data:[role],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let role = await Role.findByPk(parseInt(req.query.id.toString()));
        if(role){
          await Object.assign(role,utility.getBaseAtributes(req, 3));
          role.save();
          httpResponse.sendResponse({code:200,data:[role]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let role = await Role.findByPk(parseInt(req.body.id.toString()));
          if (role) {
            await Object.assign(role,utility.getBaseAtributes(req, 2));
            role.nombre = req.body.nombre?.toString() || "";
            role.status_id = req.body.status_id?.toString() || "";
            role.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[role]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}