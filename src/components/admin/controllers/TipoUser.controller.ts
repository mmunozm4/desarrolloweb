import { Request, Response } from 'express';
import {TipoUser,TipoUserAddModel} from '../models'
export class tipoUserController {
    public async index(req: Request, res: Response) {
        //console.log(res.locals.user);
        if (req.query!)
          if (req.query.id!)
            res.send(await TipoUser.findByPk(parseInt(req.query.id.toString())));
          else res.send(await TipoUser.findAll());
    }
    public async add(req: Request, res: Response) {
        let tipouser:TipoUserAddModel = <TipoUserAddModel>req.body;
            tipouser.actualizado = new Date();
            tipouser.actualizado_por = 1;
            tipouser.creado = new Date();
            tipouser.creado_por = 1;
        tipouser = await TipoUser.create(tipouser);
        res.send(tipouser);
      }
}