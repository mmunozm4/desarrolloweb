import { Request, Response } from "express";
import { httpResponse, utility } from "../../../../core";
import { RoleAgrupacion } from "../../models";
export class RoleAgrupacionController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:RoleAgrupacion|null  = await RoleAgrupacion.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:RoleAgrupacion[] = await RoleAgrupacion.findAll({
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  
  public async add(req: Request, res: Response) {
    try{
      let roleAgrupacion = await RoleAgrupacion.create(req.body);
      await Object.assign(roleAgrupacion,utility.getBaseAtributes(req,1));
      (await roleAgrupacion).save();
      httpResponse.sendResponse({code:200, data:[roleAgrupacion],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let roleAgrupacion = await RoleAgrupacion.findByPk(parseInt(req.query.id.toString()));
        if(roleAgrupacion){
          await Object.assign(roleAgrupacion,utility.getBaseAtributes(req, 3));
          roleAgrupacion.save();
          httpResponse.sendResponse({code:200,data:[roleAgrupacion]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let roleAgrupacion = await RoleAgrupacion.findByPk(parseInt(req.body.id.toString()));
          if (roleAgrupacion) {
            await Object.assign(roleAgrupacion,utility.getBaseAtributes(req, 2));
            roleAgrupacion.agrupacion_id = req.body.agrupacion_id?.toString() || "";
            roleAgrupacion.role_id = req.body.role_id?.toString() || "";
            roleAgrupacion.status_id = req.body.status_id?.toString() || "";
            roleAgrupacion.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[roleAgrupacion]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}