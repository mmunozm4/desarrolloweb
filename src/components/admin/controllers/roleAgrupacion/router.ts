import {Router,Application} from 'express';
import {ICore} from '../../../../core'
import {RoleAgrupacionController} from '.';
export default class tipoUserRouter extends RoleAgrupacionController implements ICore.routerInterface{
    router:Router = Router();
    constructor(protected globalApp:Application,public endPointBase:string = '/'){
        super();
        this.loadRouter();
        globalApp.use(this.router);
    }    
    loadRouter(){
        this.router.route(this.endPointBase)
            .get(this.index)
            .post(this.add)
            .delete(this.delete)
            .put(this.update);
        
    }
}