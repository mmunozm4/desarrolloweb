import { Request, Response } from "express";
import {Op} from 'sequelize';
import { httpResponse, utility } from "../../../../core";
import { Funcion } from "../../models";
export class FuncionController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:Funcion|null  = await Funcion.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:Funcion[] = await Funcion.findAll({where:{
          TRLID:{
            [Op.ne]:3
          }
        },
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async add(req: Request, res: Response) {
    try{
      let funcion = await Funcion.create(req.body);
      await Object.assign(funcion,utility.getBaseAtributes(req,1));
      (await funcion).save();
      httpResponse.sendResponse({code:200, data:[funcion],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let funcion = await Funcion.findByPk(parseInt(req.query.id.toString()));
        if(funcion){
          await Object.assign(funcion,utility.getBaseAtributes(req, 3));
          funcion.save();
          httpResponse.sendResponse({code:200,data:[funcion]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let funcion = await Funcion.findByPk(parseInt(req.body.id.toString()));
          if (funcion) {
            await Object.assign(funcion,utility.getBaseAtributes(req, 2));
            funcion.nombre = req.body.nombre?.toString() || "";
            funcion.acronimo = req.body.acronimo?.toString() || "";
            funcion.statusId = req.body.statusId?.toString() || "";
            funcion.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[funcion]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}