import {Router,Application} from 'express';
import {ICore} from '../../../../core'
import {FuncionController} from '.';
export default class sedeRouter extends FuncionController implements ICore.routerInterface{
    router:Router = Router();
    constructor(protected globalApp:Application,public endPointBase:string = '/'){
        super();
        this.loadRouter();
        globalApp.use(this.router);
    }    
    loadRouter(){
        this.router.route(this.endPointBase)
            .get(this.index)
            .post(this.add)
            .delete(this.delete)
            .put(this.update);
        
    }
}