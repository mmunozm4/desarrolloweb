import { Request, Response } from "express";
import {Op} from 'sequelize';
import { httpResponse, utility } from "../../../../core";
import { TRL } from "../../models";
export class TRLController {
  public async index(req: Request, res: Response) {
    try{    
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:TRL|null  = await TRL.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:TRL[] = await TRL.findAll({
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      console.log(err);
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  
  public async add(req: Request, res: Response) {
    try{
      let trl = await TRL.create(req.body);
      trl = <TRL>{...trl,...utility.getBaseAtributes(req,1)};
      (await trl).save();
      httpResponse.sendResponse({code:200, data:[trl],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let trl = await TRL.findByPk(parseInt(req.query.id.toString()));
        if(trl){
          trl = <TRL>{
            ...trl,
            ...utility.getBaseAtributes(req, 3),
          };     
          trl.save();
          httpResponse.sendResponse({code:200,data:[trl]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let trl = await TRL.findByPk(parseInt(req.body.id.toString()));
          if (trl) {
            trl.creado_por = 1;
            trl.actualizado_por = 1;
            trl.actualizado = new Date();
            trl.nombre = req.query.nombre?.toString() || "";
            trl.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[trl]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}
