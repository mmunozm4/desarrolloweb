import { accesoDTO } from "./acceso.dto";
import { Request, Response } from "express";
import { Op } from "sequelize";
import { utility, httpResponse, ICore } from "../../../../core";
import { conDBOppor } from "../../../../instances";


import { Acceso } from "../../models";
export class accesoController extends accesoDTO {
  public async get(req: Request, res: Response) {
    let pagination = utility.getPaginationParams(req.query);
    if (req.query!)
      if (req.query.id!)
        res.send(
          await Acceso.findByPk(parseInt(req.query.id.toString()), {
            ...pagination,
            attributes: accesoDTO.columnsBase,
          })
        );
      else
        res.send(
          await Acceso.findAll({
            ...pagination,
            
            attributes: accesoDTO.columnsBase,
          })
        );
  }
  public async index(req: Request, res: Response) {
    try {
      let pagination = utility.getPaginationParams(req.query);
      if (req.query!)
        if (req.query.id!) {
          let data: Acceso | null = await Acceso.findByPk(
            parseInt(req.query.id.toString()),
            {
              ...pagination,
              raw: true,
            }
          );
          httpResponse.sendResponse(
            { code: 200, data: data ? [data] : [], mensaje: "" },
            res
          );
        } else {
          let data: Acceso[] = await Acceso.findAll({
            where: {
              TRLID: {
                [Op.ne]: 3,
              },
            },
            ...pagination,
            raw: true,
          });
          httpResponse.sendResponse({ code: 200, data, mensaje: "" }, res);
        }
    } catch (err: any) {
      console.log(err);
      httpResponse.sendResponse({ code: 500, mensaje: err.message }, res);
    }
  }
  public async add(req: Request, res: Response) {
    try{
      let acceso = await Acceso.create(req.body);
      await Object.assign(acceso,utility.getBaseAtributes(req,1));
      (await acceso).save();
      httpResponse.sendResponse({code:200, data:[acceso],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let acceso = await Acceso.findByPk(parseInt(req.query.id.toString()));
        if(acceso){
          await Object.assign(acceso,utility.getBaseAtributes(req, 3));
          acceso.save();
          httpResponse.sendResponse({code:200,data:[acceso]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let acceso = await Acceso.findByPk(parseInt(req.body.id.toString()));
          if (acceso) {
            await Object.assign(acceso,utility.getBaseAtributes(req, 2));
            acceso.status_id = parseInt(req.body.status_id?.toString() || "");
            acceso.pagina_id = parseInt(req.body.pagina_id?.toString() || "");
            acceso.metodo_id = parseInt(req.body.metodo_id?.toString() || "");
            acceso.descripcion = req.body.descripcion?.toString() || "";
            acceso.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[acceso]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}
