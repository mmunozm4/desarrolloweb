import { Request, Response } from "express";

import {TRL,Sistema,SisConfig, TipoUser,User,Pagina,Funcion,TipoMetodo,Acceso,Role,AccesoRole,RoleAgrupacion,GrupoRole,TipoUserView} from "../../models";
import {
  Persona } from "../../../persona/models";
import {utility} from '../../../../core';

export class initController{
    public static basicView = TipoUserView;

    public static async initData(req: Request, res: Response) {
        let basicElements = {
          creado: new Date(),
          actualizado: new Date(),
          creado_por: 1,
          actualizado_por: 1,
        };
        let trlActivo =
          (await TRL.findOne({
            where: {
              nombre: "Activo",
            },
            attributes: initController.basicView,
          })) ||
          (await TRL.create({
            nombre: "Activo",
            ...basicElements,
          }));
    
        let trlHistorico =
          (await TRL.findOne({
            where: {
              nombre: "Historico",
            },
            attributes: initController.basicView,
          })) ||
          (await TRL.create({
            nombre: "Historico",
            ...basicElements,
          }));
    
        let trlEliminado =
          (await TRL.findOne({
            where: {
              nombre: "Eliminado",
            },
            attributes: initController.basicView,
          })) ||
          (await TRL.create({
            nombre: "Eliminado",
            ...basicElements,
          }));
    
        let TUAdmin =
          (await TipoUser.findOne({
            where: {
              nombre: "Admin",
            },
            attributes: initController.basicView,
          })) ||
          (await TipoUser.create({
            nombre: "Admin",
            ...basicElements,
            TRLID: trlActivo.id,
          }));
    
        let TURestringido =
          (await TipoUser.findOne({
            where: {
              nombre: "Restringido",
            },
            attributes: initController.basicView,
          })) ||
          (await TipoUser.create({
            nombre: "Restringido",
            ...basicElements,
            TRLID: trlActivo.id,
          }));    
        let SistemaCoresys =
          (await Sistema.findOne({
            where: {
              nombre: "CORESYS",
            },
            attributes: initController.basicView,
          })) ||
          (await Sistema.create({
            nombre: "CORESYS",
            acronimo:"CORE",
            statusId: 1,
            habilitado: true,
            ...basicElements,
          }));
    
        let SistemaPeople =
          (await Sistema.findOne({
            where: {
              nombre: "PEOPLE",
            },
            attributes: initController.basicView,
          })) ||
          (await Sistema.create({
            nombre: "PEOPLE",
            acronimo:"PEOP",
            statusId: 1,
            habilitado: true,
            ...basicElements,
          }));
    
        let SistemaOpp =
          (await Sistema.findOne({
            where: {
              nombre: "OPP",
            },
            attributes: initController.basicView,
          })) ||
          (await Sistema.create({
            nombre: "OPP",
            acronimo:"OPP",
            statusId: 1,
            habilitado: true,
            ...basicElements,
          }));
    
        let PaginaTU =
          (await Pagina.findOne({
            where: {
              nombre: "TIPO-USER",
            },
            attributes: initController.basicView,
          })) ||
          (await Pagina.create({
            nombre: "TIPO-USER",
            acronimo:"TU",
            status_id: 1,
            url: "/admin/tipouser",
            api: true,
            secure: true,
            sistema_id: 1,
            ...basicElements,
            nivel: 1,
          }));
    
        let TMGet =
          (await TipoMetodo.findOne({
            where: {
              nombre: "GET",
            },
            attributes: initController.basicView,
          })) ||
          (await TipoMetodo.create({
            nombre: "GET",
            statusId: 1,
            ...basicElements,
            TRLID: trlActivo.id,
          }));
    
        let TMPOST =
          (await TipoMetodo.findOne({
            where: {
              nombre: "POST",
            },
            attributes: initController.basicView,
          })) ||
          (await TipoMetodo.create({
            nombre: "POST",
            statusId: 1,
            ...basicElements,
            TRLID: trlActivo.id,
          }));
    
        let TMUPDATE =
          (await TipoMetodo.findOne({
            where: {
              nombre: "UPDATE",
            },
            attributes: initController.basicView,
          })) ||
          (await TipoMetodo.create({
            nombre: "UPDATE",
            statusId: 1,
            ...basicElements,
            TRLID: trlActivo.id,
          }));
    
        let TMDELETE =
          (await TipoMetodo.findOne({
            where: {
              nombre: "DELETE",
            },
            attributes: initController.basicView,
          })) ||
          (await TipoMetodo.create({
            nombre: "DELETE",
            statusId: 1,
            ...basicElements,
            TRLID: trlActivo.id,
          }));
    
        /*let AccesoTU = await Acceso.findByPk( where:{
          cod: "DELETE"
        })*/
    
        res.send({
          trlActivo,
          trlHistorico,
          trlEliminado,
          TUAdmin,
          TURestringido,
          SistemaCoresys,
          SistemaPeople,
          SistemaOpp,
          PaginaTU,
          TMGet,
          TMPOST,
          TMUPDATE,
          TMDELETE,
        });
      }
}