import { Request, Response } from "express";

import {TRL,Sistema,SisConfig, TipoUser,User,Pagina,Funcion,TipoMetodo,Acceso,Role,AccesoRole,RoleAgrupacion,GrupoRole,TipoUserView} from "../../models";
import {
  Persona
} from "../../../persona/models";

import {utility} from '../../../../core';

export class syncController {

  public static basicView = TipoUserView;

  public async sync(req: Request, res: Response) {
    try {
      //*********************************************************** */
      //*Esquema persona */
      //*********************************************************** */
      
      await Persona.sync();
      
      //*********************************************************** */
      //*FIN Esquema persona */
      //*********************************************************** */

      //*********************************************************** */
      //*Esquema coresys */
      //*********************************************************** */
      await TRL.sync();
      await Sistema.sync();
      await SisConfig.sync();
      await TipoUser.sync();
      await User.sync();
      await Persona.sync();
      await Pagina.sync();
      await TipoMetodo.sync();
      await Funcion.sync();
      await Acceso.sync();
      await Role.sync();
      await GrupoRole.sync();
      await RoleAgrupacion.sync();
      await AccesoRole.sync();
      //*********************************************************** */
      //*FIN Esquema coresys */
      //*********************************************************** */

      res.send("sincronizacion realizada");
    } catch (ex) {
      res.send("error al sincronizar" + ex);
    }
  }
  public async syncForce(req: Request, res: Response) {
    try {
      //*********************************************************** */
      //*Esquema persona */
      //*********************************************************** */
      
      await Persona.sync({force:true});
     
      //*********************************************************** */
      //*FIN Esquema persona */
      //*********************************************************** */

      //*********************************************************** */
      //* Esquema coresys */
      //*********************************************************** */
      await TRL.sync({ force: true });
      await Sistema.sync({ force: true });
      await SisConfig.sync({ force: true });
      await TipoUser.sync({ force: true });
      await User.sync({ force: true });
      await Persona.sync({ force: true });
      await Pagina.sync({ force: true });
      await TipoMetodo.sync({ force: true });
      await Funcion.sync({ force: true });
      await Acceso.sync({ force: true });
      await Role.sync({ force: true });
      await GrupoRole.sync({ force: true });
      await RoleAgrupacion.sync({ force: true });
      await AccesoRole.sync({ force: true });
      //*********************************************************** */
      //*FIN Esquema coresys */
      //*********************************************************** */
      res.send("sincronizacion Forzada");
    } catch (ex) {
      res.send("error al sincronizar" + ex);
    }
  }
  public async syncAlter(req: Request, res: Response) {
    try {
      //*********************************************************** */
      //*Esquema persona */
      //*********************************************************** */
     
      await Persona.sync({alter:true});
      
      //*********************************************************** */
      //*FIN Esquema persona */
      //*********************************************************** */

      //*********************************************************** */
      //* Esquema coresys */
      //*********************************************************** */
      await TRL.sync({ alter: true });
      await Sistema.sync({ alter: true });
      await SisConfig.sync({ alter: true });
      await TipoUser.sync({ alter: true });
      await User.sync({ alter: true });
      await Persona.sync({ alter: true });
      await Pagina.sync({ alter: true });
      await TipoMetodo.sync({ alter: true });
      await Funcion.sync({ alter: true });
      await Acceso.sync({ alter: true });
      await Role.sync({ alter: true });
      await GrupoRole.sync({ alter: true });
      await RoleAgrupacion.sync({ alter: true });
      await AccesoRole.sync({ alter: true });
      //*********************************************************** */
      //*FIN Esquema coresys */
      //*********************************************************** */
      res.send("sincronizacion de campos realizada");
    } catch (ex) {
      res.send("error al sincronizar" + ex);
    }
  }
}
