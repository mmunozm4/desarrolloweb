import { Request, Response } from "express";
import { httpResponse, utility } from "../../../../core";
import { SisConfig } from "../../models";
export class SisConfigController {
  public async index(req: Request, res: Response) {
    try{  
    let pagination = utility.getPaginationParams(req.query);    
    if (req.query!)
      if (req.query.id!){
        let data:SisConfig|null  = await SisConfig.findByPk(parseInt(req.query.id.toString()),{
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data:data?[data]:[],mensaje:''},res);
      }
      else {
        let data:SisConfig[] = await SisConfig.findAll({
          ...pagination,
          raw:true
        });
        httpResponse.sendResponse({code:200, data,mensaje:''},res);
      }
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  
  public async add(req: Request, res: Response) {
    try{
      let sisConfig = await SisConfig.create(req.body);
      await Object.assign(sisConfig,utility.getBaseAtributes(req,1));
      (await sisConfig).save();
      httpResponse.sendResponse({code:200, data:[sisConfig],mensaje:''},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async delete(req: Request, res: Response) {
    try{
    if (req.query!)
      if (req.query.id!) {
        let sisConfig = await SisConfig.findByPk(parseInt(req.query.id.toString()));
        if(sisConfig){
          await Object.assign(sisConfig,utility.getBaseAtributes(req, 3));
          sisConfig.save();
          httpResponse.sendResponse({code:200,data:[sisConfig]},res);
        } else httpResponse.sendResponse({code:404},res);
      } else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
  public async update(req: Request, res: Response) {
    try{
      if (req.body!)
        if (req.body.id!) {
          let sisConfig = await SisConfig.findByPk(parseInt(req.body.id.toString()));
          if (sisConfig) {
            await Object.assign(sisConfig,utility.getBaseAtributes(req, 2));
            sisConfig.config = req.body.config?.toString() || "";
            sisConfig.sistema_id = req.body.sistema_id?.toString() || "";
            sisConfig.statusId = req.body.statusId?.toString() || "";
            sisConfig.save();
          }else httpResponse.sendResponse({code:404},res);
          httpResponse.sendResponse({code:200,data:[sisConfig]},res);
        } else httpResponse.sendResponse({code:404},res);
      else httpResponse.sendResponse({code:404},res);
    }catch(err:any){
      httpResponse.sendResponse({code:500,mensaje:err.message},res);
    }
  }
}