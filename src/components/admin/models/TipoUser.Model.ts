import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface TipoUserAttributes {
  id?: number;
  nombre: string;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export const TipoUserView = ['id','nombre'];

export interface TipoUserAddModel {
  nombre: string;
  creado: Date;
  creado_por: number;
  actualizado: Date;
  actualizado_por: number;
  TRLID: number;
}

class TipoUser extends Model<TipoUserAttributes, TipoUserAddModel> {
  id!: number;
  nombre!: string;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

TipoUser.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "tipo_user",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// TipoUser.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// TipoUser.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { TipoUser };
