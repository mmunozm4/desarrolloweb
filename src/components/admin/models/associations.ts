import {TRL,Sistema,SisConfig, TipoUser,User,Pagina,Funcion,TipoMetodo,Acceso,Role,AccesoRole,RoleAgrupacion,GrupoRole} from './';
import {
    Persona
} from '../../persona/models';

TRL.hasMany(TipoUser,{foreignKey:'TRLID',sourceKey:'id'});

Sistema.hasMany(SisConfig,{foreignKey:'sistema_id',sourceKey:'id'});
Sistema.hasMany(Pagina,{foreignKey:'sistema_id',sourceKey:'id'});

SisConfig.belongsTo(Sistema,{foreignKey:'sistema_id', targetKey:'id'});

Pagina.hasMany(Pagina,{foreignKey:'pagina_id',sourceKey:'id'});
Pagina.hasMany(Acceso,{foreignKey:'pagina_id',sourceKey:'id'});
Pagina.belongsTo(Sistema,{foreignKey:'sistema_id',targetKey:'id'});
Pagina.belongsTo(Pagina,{foreignKey:'pagina_id',targetKey:'id'});


Funcion.hasMany(Acceso,{foreignKey:'funcion_id',sourceKey:'id'});

TipoMetodo.hasMany(Acceso,{foreignKey:'metodo_id',sourceKey:'id'});

Acceso.hasMany(AccesoRole,{foreignKey:'cod',sourceKey:'cod'})
Acceso.belongsTo(Pagina,{foreignKey:'pagina_id',targetKey:'id'});
Acceso.belongsTo(Funcion,{foreignKey:'funcion_id',targetKey:'id'});
Acceso.belongsTo(TipoMetodo,{foreignKey:'metodo_id',targetKey:'id'});


Role.hasMany(AccesoRole,{foreignKey:'role_id',sourceKey:'id'});
Role.hasMany(RoleAgrupacion,{foreignKey:'role_id',sourceKey:'id'});

AccesoRole.belongsTo(Role,{foreignKey:'role_id',targetKey:'id'});
AccesoRole.belongsTo(Acceso,{foreignKey:'cod',targetKey:'cod'});


GrupoRole.hasMany(RoleAgrupacion,{foreignKey:'role_id',sourceKey:'id'});


RoleAgrupacion.belongsTo(Role,{foreignKey:'role_id',targetKey:'id'})
RoleAgrupacion.belongsTo(GrupoRole,{foreignKey:'grupo_id',targetKey:'id'})




TipoUser.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
User.belongsTo(TipoUser,{foreignKey:'tipo_user_id', targetKey:'id'});
//*********************************************************** */
//*Relacion de esquema PERSONA */
//*********************************************************** */
//FK de  persona con  Ocupacion
// Ocupacion.hasMany(Persona,{foreignKey:'ocupacion_id',sourceKey:'id'});
//Persona.belongsTo(Ocupacion,{foreignKey:'ocupacion_id', targetKey:'id'}) 