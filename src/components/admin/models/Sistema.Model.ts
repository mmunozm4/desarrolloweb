import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface SistemaAttributes {
  id?: number;
  nombre: string;
  descripcion: string;
  acronimo:string;
  expiracion: Date;
  habilitado: Boolean;
  statusId: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface SistemaAddModel {
  nombre: string;
  statusId: number;
  acronimo:string;
  descripcion?: string;
  expiracion?: Date;
  habilitado: Boolean;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
}

class Sistema extends Model<SistemaAttributes, SistemaAddModel> {
  id!: number;
  nombre!: string;
  acronimo!:string;
  statusId!: number;
  descripcion!: string;
  expiracion!: Date;
  habilitado!: Boolean;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

Sistema.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    acronimo: DataTypes.STRING(4),
    descripcion: DataTypes.TEXT,
    expiracion: DataTypes.DATE,
    habilitado: DataTypes.BOOLEAN,
    statusId: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "sistema",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// Sistema.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// Sistema.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { Sistema };
