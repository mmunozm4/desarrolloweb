import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface RoleAttributes {
  id?: number;
  nombre: string;
  status_id: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface RoleAddModel {
  nombre: string;
  status_id: number;
}

class Role extends Model<RoleAttributes, RoleAddModel> {
  id!: number;
  nombre!: string;
  status_id!: number;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

Role.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    status_id: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "role",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// Role.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// Role.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { Role };
