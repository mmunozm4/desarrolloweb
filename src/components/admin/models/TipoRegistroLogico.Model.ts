import { Model, DataTypes, NOW} from 'sequelize';
import { conDBOppor } from '../../../instances';

export interface TRLAttributes {
    id?: number;
    nombre: string;
    creado?: Date;
    creado_por?: number;
    actualizado?: Date;
    actualizado_por?: number;
    eliminado?: Date;
    eliminado_por?: number
}
export interface TRLAddModel{
    nombre:string;
    creado: Date;
    creado_por: number;
    actualizado: Date;
    actualizado_por: number;
}

export interface TRLView{
    id:number;
    nombre:string;
    creado: Date;
    creado_por: number;
    actualizado: Date;
    actualizado_por: number;
}

class TRL extends Model<TRLAttributes,TRLAddModel>{ 
    id!:number;
    nombre!:string;
    creado!: Date;
    creado_por!: number;
    actualizado!: Date;
    actualizado_por!: number;
    eliminado!: Date;
    eliminado_por!: number;
}

TRL.init( {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    nombre: DataTypes.STRING,
    creado: {
        type: DataTypes.DATE,
        defaultValue: NOW
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
        type: DataTypes.DATE,
        defaultValue: NOW
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER
},
{
    tableName: "tipo_registro_logico",
    sequelize:conDBOppor,
    timestamps: false,
    schema: 'coresys'
     // passing the `sequelize` instance is required
});


export {TRL};