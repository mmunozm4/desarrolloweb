import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface AccesoRoleAttributes {
  id?: number;
  nombre: string;
  status_id: number;
  cod: string;
  role_id: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface AccesoRoleAddModel {
  nombre: string;
  status_id: number;
  cod: string;
  role_id: number;
}

class AccesoRole extends Model<AccesoRoleAttributes, AccesoRoleAddModel> {
  id!: number;
  nombre!: string;
  status_id!: number;
  cod!: string;
  role_id!: number;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

AccesoRole.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    status_id: DataTypes.INTEGER,
    cod: DataTypes.STRING,
    role_id: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "acceso_role",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// AccesoRole.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// AccesoRole.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { AccesoRole };
