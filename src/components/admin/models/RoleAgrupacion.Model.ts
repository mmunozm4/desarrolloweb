import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface RoleAgrupacionAttributes {
  id?: number;
  status_id: number;
  role_id: number;
  agrupacion_id: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface RoleAgrupacionAddModel {
  status_id: number;
  role_id: number;
  agrupacion_id: number;
}

class RoleAgrupacion extends Model<RoleAgrupacionAttributes, RoleAgrupacionAddModel> {
  id!: number;
  status_id!: number;
  role_id!: number;
  agrupacion_id!: number;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

RoleAgrupacion.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },

    status_id: DataTypes.INTEGER,
    role_id: DataTypes.INTEGER,
    agrupacion_id: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "role_agrupacion",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// RoleAgrupacion.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// RoleAgrupacion.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { RoleAgrupacion };
