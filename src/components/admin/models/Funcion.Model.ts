import { Model, DataTypes, NOW } from 'sequelize';
import { conDBOppor } from '../../../instances';

export interface FuncionAttributes {
    id?: number
    nombre: string,
    statusId: number,
    acronimo:string;
    creado?: Date,
    creado_por?: number,
    actualizado?: Date,
    actualizado_por?: number,
    eliminado?: Date,
    eliminado_por?: number,
    TRLID?: number
}

export interface FuncionAddModel {
    nombre: string;
    statusId: number;
}

class Funcion extends Model <FuncionAttributes,FuncionAddModel>{
    id!: number;
    nombre!: string;
    acronimo!:string;
    statusId!: number;
    creado!: Date;
    creado_por!: number;
    actualizado!: Date;
    actualizado_por!: number;
    eliminado!: Date;
    eliminado_por!: number;
    TRLID!: number;
}

Funcion.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nombre: DataTypes.STRING,
        acronimo: DataTypes.STRING(4),
        statusId: DataTypes.INTEGER,
        creado: {
            type: DataTypes.DATE,
            defaultValue: NOW
        },
        creado_por: DataTypes.INTEGER,
        actualizado: {
            type: DataTypes.DATE,
            defaultValue: NOW
        },
        actualizado_por: DataTypes.INTEGER,
        eliminado: DataTypes.DATE,
        eliminado_por: DataTypes.INTEGER,
        TRLID: DataTypes.INTEGER
    }, {
        tableName: "funcion",
        sequelize:conDBOppor,    
        timestamps: false,
        schema: 'coresys'
    }
);

// Funcion.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// Funcion.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export {Funcion};