export * from "./TipoRegistroLogico.Model";
export * from "./Sistema.Model";
export * from "./SisConfig.Model";
export * from "./TipoUser.Model";
export * from "./User.Model";
export * from "./Pagina.Model";
export * from "./Funcion.Model";
export * from "./TipoMetodo.Model";
export * from "./Acceso.Model";
export * from "./Role.Model";
export * from "./GrupoRole.Model";
export * from "./RoleAgrupacion.Model";
export * from "./AccesoRole.Model";

import "./associations";
