import { Model, DataTypes, NOW} from "sequelize";
import { conDBOppor } from "../../../instances";

export const userConcep = ['id','username'];
export interface userAttributes {
  id?: number;
  username: string;
  password: string;
  persona_id:number;
  status_id: number;
  tipo_user_id: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface userAdd {
  username: string;
  password?: string;
  persona_id:number;
  status_id: number;
  tipo_user_id: number;
}
export interface userValidator{
  errors: string;
  valid: Boolean;
}
export const userAttributesForToken = ["id","username","password","persona_id","tipo_user_id","status_id"];

export interface UserViewModel {
  username: string;
  persona_id:number;
  status_id: number;
  tipo_user_id: number;
}
class User extends Model<userAttributes, userAdd> {
  id?: number;
  username!: string;
  password?: string;
  persona_id!: number;
  status_id!: number;
  tipo_user_id!: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

User.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    username: DataTypes.STRING(50),
    password: DataTypes.TEXT,
    persona_id: DataTypes.INTEGER,
    status_id: DataTypes.INTEGER,
    tipo_user_id: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "user",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);
//User.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});
//User.belongsTo(TipoUser,{foreignKey:'tipo_user_id', targetKey:'id'});

export { User };
