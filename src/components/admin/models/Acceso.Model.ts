import { Model, DataTypes, NOW } from 'sequelize';
import { conDBOppor } from '../../../instances';

export interface AccesoAttributes {
    cod?: string;
    status_id: number;
    pagina_id: number;
    metodo_id: number;
    funcion_id: number;
    descripcion:string;
    creado?: Date;
    creado_por?: number;
    actualizado?: Date;
    actualizado_por?: number;
    eliminado?: Date;
    eliminado_por?: number;
    TRLID?: number;
}

export interface AccesoAddModel {
    status_id: number;
    pagina_id?: number;
    metodo_id?: number;
    funcion_id?: number;
    descripcion:string;
}

class Acceso extends Model <AccesoAttributes,AccesoAddModel>{
    cod!: string;
    descripcion!:string;
    status_id!: number;
    pagina_id!: number;
    metodo_id!: number;
    funcion_id!: number;
    creado!: Date;
    creado_por!: number;
    actualizado!: Date;
    actualizado_por!: number;
    eliminado!: Date;
    eliminado_por!: number;
    TRLID!: number;
}

Acceso.init(
    {
        cod: {
            type: DataTypes.STRING(20),
            primaryKey: true
        },
        descripcion:DataTypes.TEXT,
        status_id: DataTypes.INTEGER,
        pagina_id: DataTypes.INTEGER,
        metodo_id: DataTypes.INTEGER,
        funcion_id: DataTypes.INTEGER,
        
        creado: {
            type: DataTypes.DATE,
            defaultValue: NOW
        },
        creado_por: DataTypes.INTEGER,
        actualizado: {
            type: DataTypes.DATE,
            defaultValue: NOW
        },
        actualizado_por: DataTypes.INTEGER,
        eliminado: DataTypes.DATE,
        eliminado_por: DataTypes.INTEGER,
        TRLID: DataTypes.INTEGER
    }, {
        tableName: "acceso",
        sequelize:conDBOppor,    
        timestamps: false,
        schema: 'coresys'
    }
);

// Acceso.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// Acceso.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export {Acceso};