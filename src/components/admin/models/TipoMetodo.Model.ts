import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface TipoMetodoAttributes {
  id?: number;
  nombre: string;
  statusId: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface TipoMetodoAddModel {
  nombre: string;
  statusId: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  TRLID: number;
}

class TipoMetodo extends Model<TipoMetodoAttributes, TipoMetodoAddModel> {
  id!: number;
  nombre!: string;
  statusId!: number;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

TipoMetodo.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING,
    statusId: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "tipo_metodo",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// TipoMetodo.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// TipoMetodo.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { TipoMetodo };
