import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface PaginaAttributes {
  id?: number;
  nombre: string;
  acronimo:string;
  status_id: number;
  descripcion?: string;
  url?: string;
  api?: Boolean;
  secure?: Boolean;
  sistema_id?: number;
  pagina_id?: number;
  config?: string;
  nivel?: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface PaginaAddModel {
  nombre: string;
  acronimo:string;
  status_id: number;
  descripcion?: string;
  url: string;
  api: Boolean;
  secure: Boolean;
  sistema_id: number;
  pagina_id?: number;
  config?: string;
  nivel: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
}

class Pagina extends Model<PaginaAttributes, PaginaAddModel> {
  id!: number;
  nombre!: string;
  acronimo!:string;
  statusId!: number;
  descripcion!: string;
  url!: string;
  api!: Boolean;
  secure!: Boolean;
  sistema_id!: number;
  pagina_id!: number;
  config!: string;
  nivel!: number;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

Pagina.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: DataTypes.STRING(100),
    acronimo:DataTypes.STRING(4),
    status_id: DataTypes.INTEGER,
    descripcion: DataTypes.TEXT,
    url: DataTypes.STRING,
    api: DataTypes.BOOLEAN,
    secure: DataTypes.BOOLEAN,
    sistema_id: DataTypes.INTEGER,
    pagina_id: DataTypes.INTEGER,
    config: DataTypes.TEXT,
    nivel: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "pagina",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// Pagina.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// Pagina.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { Pagina };
