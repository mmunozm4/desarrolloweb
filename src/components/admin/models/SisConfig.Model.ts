import { Model, DataTypes, NOW } from "sequelize";
import { conDBOppor } from "../../../instances";

export interface SisConfigAttributes {
  id?: number;
  config: string;
  statusId: number;
  sistema_id: number;
  creado?: Date;
  creado_por?: number;
  actualizado?: Date;
  actualizado_por?: number;
  eliminado?: Date;
  eliminado_por?: number;
  TRLID?: number;
}

export interface SisConfigAddModel {
  config: string;
  statusId: number;
  sistema_id: number;
}

class SisConfig extends Model<SisConfigAttributes, SisConfigAddModel> {
  id!: number;
  config!: string;
  statusId!: number;
  sistema_id!: number;
  creado!: Date;
  creado_por!: number;
  actualizado!: Date;
  actualizado_por!: number;
  eliminado!: Date;
  eliminado_por!: number;
  TRLID!: number;
}

SisConfig.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    config: DataTypes.TEXT,
    statusId: DataTypes.INTEGER,
    sistema_id: DataTypes.INTEGER,
    creado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    creado_por: DataTypes.INTEGER,
    actualizado: {
      type: DataTypes.DATE,
      defaultValue: NOW,
    },
    actualizado_por: DataTypes.INTEGER,
    eliminado: DataTypes.DATE,
    eliminado_por: DataTypes.INTEGER,
    TRLID: DataTypes.INTEGER,
  },
  {
    tableName: "sisconfig",
    sequelize: conDBOppor,
    timestamps: false,
    schema: "coresys",
  }
);

// SisConfig.hasMany(User,{foreignKey:'tipo_user_id',sourceKey:'id'});
// SisConfig.belongsTo(TRL,{foreignKey:'TRLID',targetKey:'id'});

export { SisConfig };
