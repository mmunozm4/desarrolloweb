import {Router,Application,Request,Response} from 'express';
import {ICore} from '../../../core'
//import * as mtokens from '../middlewares/validacionToken'
import {validadcionToken} from '../middlewares/validacionToken'

import {indexController} from '../controllers/index.controller';
import {conDBOppor} from '../../../instances'
export default class indexRoute extends indexController implements ICore.routerInterface{
    router:Router = Router();
    constructor(protected globalApp:Application,public endPointBase:string = '/'){
        super();
        this.loadRouter();
        globalApp.use(this.router);
    }    
    loadRouter(){
        this.router.route(this.endPointBase).get(this.index);
        this.router.route(this.endPointBase+'/datareport').all(validadcionToken.validartoken).post(this.getDataReport);
    }
}