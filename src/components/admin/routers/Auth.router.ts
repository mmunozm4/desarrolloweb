import { Router, Application } from "express";
import { ICore } from "../../../core";
import { authController } from "../controllers/auth.controller";
import {authorizationMiddleware} from "../middlewares"

export default class authRouter
  extends authController
  implements ICore.routerInterface {
  router: Router = Router();
  constructor(
    protected globalApp: Application,
    public endPointBase: string = "/"
  ) {
    super();
    this.loadRouter();
    globalApp.use(this.router);
  }
  loadRouter() {
    this.router
      .route(this.endPointBase + "/registerbasic")
      .post(this.registerBasic);

    this.router.route(this.endPointBase + "/login").post(this.login);
  }
}
