import {Router,Application} from 'express';

import {ICore} from '../../../core'
import {syncController} from '../controllers/sync/sync.Controller';
import {initController} from '../controllers/sync/init.Controller';
export default class syncRouter extends syncController implements ICore.routerInterface {
    router:Router = Router();
    //endPointBase:string='';
    constructor(protected globalApp:Application,public endPointBase:string = '/'){
        super();
        this.loadRouter();
        globalApp.use(this.router);
    }    
    loadRouter(){
        this.router.route(this.endPointBase).get(this.sync);
        this.router.route(this.endPointBase+'force').get(this.syncForce);
        this.router.route(this.endPointBase+'alter').get(this.syncAlter);
        this.router.route(this.endPointBase+'/initdata').get(initController.initData);
    }
}