import {Router,Application} from 'express';

import {ICore} from '../../../core'
import {tipoUserController} from '../controllers/TipoUser.controller';
import {authorizationMiddleware} from '../middlewares';

export default class TipoUserRouter extends tipoUserController implements ICore.routerInterface{
    router:Router = Router();
    //endPointBase:string='';
    constructor(protected globalApp:Application,public endPointBase:string = '/'){
        super();
        this.loadRouter();
        globalApp.use(this.router);
    }    
    loadRouter(){
        this.router.route(this.endPointBase)
            .all(authorizationMiddleware.checkLogin)
            .get(authorizationMiddleware.checkAuthorization,this.index)
            .post(this.add);
    }
}