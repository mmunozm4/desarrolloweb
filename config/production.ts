export default {
    server:{
        port: parseInt(process.env.PORT!) ||80
    },
    session:{
        EXPIRES_IN:"2H"
    },
    dataBases:{
        Oppor:{
            db:String(process.env.DBOPPORTUNA)||'',
            user:String(process.env.USEROPPORTUNA)||'',
            pass:String(process.env.PASSOPPORTUNA)||'',
            params:{
                host:String(process.env.HOSTDBOPPORTUNA)||'',
                dialect:String(process.env.DIALECTOPPORTUNA)||''
            }
        }
    }
}