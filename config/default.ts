import *  as path from 'path';
export default {
  app: {
    language: "ES",
  },
  server: {
    port: parseInt(process.env.PORT!) || 5000,
  },
  initConfig: {
    APP_ROOT: path.resolve(process.cwd(), "./src"),
    APP_ROOT_PROYECT: path.resolve(process.cwd()),
    APP_ROOT_VIEWS: path.resolve(process.cwd(), "./src/views"),
    CODE_ERROR: process.env.CODE_ERROR || "OPP",
    HTTPCODERESPONSE: !!process.env.HTTPCODERESPONSE || false,
    LENGTH_CODE: parseInt(process.env.LENGTH_CODE!) || 5,
  },
  session: {
    EXPIRES_IN: "2H",
    JWTSECRET: process.env.JWTSECRET || "0.rfyj3n9nzh",
    SALTROUNDS: 12,
  },
  dataBases: {
    Oppor: {
      db: process.env.DBOPPORTUNA || "Opportuna",
      user: process.env.USEROPPORTUNA || "UOpportuna",
      pass: process.env.PASSOPPORTUNA || "UOpportuna2021",
      params: {
        host: process.env.HOSTDBOPPORTUNA || "localhost",
        dialect: process.env.DIALECTOPPORTUNA || "postgres",
      },
    },
  },
  pdf: {
    Letter: {
      height: "11in", // allowed units: mm, cm, in, px
      width: "8.5in", // allowed units: mm, cm, in, px
      header: {
        height: "30mm",
      },
      footer: {
        height: "15mm",
      },
    },
    Legal: {
      height: "13.38in", // allowed units: mm, cm, in, px
      width: "8.5in", // allowed units: mm, cm, in, px
      header: {
        height: "30mm",
      },
      footer: {
        height: "28mm",
      },
    },
    header: {
      header: {
        height: "30mm",
      },
    },
  },
};